import React from 'react';
import ReactDOM from 'react-dom';

var placeholder = document.createElement("section");
placeholder.className = "placeholder";

class List2 extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            items1: [{ name1: "", id1: '' }],
            items2: [{ name2: "", id2: '' }]
        };
        this.addChange1 = this.addChange1.bind(this);
        this.onClickHandler2 = this.onClickHandler2.bind(this);
        this.addChange1 = this.addChange1.bind(this);
        this.onClickHandler2 = this.onClickHandler2.bind(this);
    }

    addChange1 = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }
    addChange2 = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    onClickHandler1 = (event) => {
        this.setState({
            items1: [...this.state.items1, { name1: this.state.name11, id1: "123" }]
        });
    }
    onClickHandler2 = (event) => {
        this.setState({
            items2: [...this.state.items2, { name2: this.state.name22, id2: "000" }]
        });
    }

    dragStart(e) {
        this.dragged = e.currentTarget;
        e.dataTransfer.effectAllowed = 'move';
        e.dataTransfer.setData('text/html', this.dragged);
    }

    dragEnd(e) {
        this.dragged.style.display = 'block';
        if (placeholder.parentNode) {
            if (this.dragged.parentNode &&
                this.dragged.parentNode.children &&
                this.dragged.parentNode.children.length) {
                let matched = false;
                for (let item of this.dragged.parentNode.children) {
                    if (item && item.nodeName && item.nodeName === 'SECTION') {
                        matched = true;
                        break;
                    }
                }
                if (matched) {
                    this.dragged.parentNode.removeChild(placeholder);
                    // update state
                    var data = this.state.items1;
                    var from = Number(this.dragged.dataset.id);
                    var to = Number(this.over.dataset.id);
                    // if (from < to) to--;
                    data.splice(to, 0, data.splice(from, 1)[0]);
                    this.setState({ items1: data });
                }

            }
        }
    }

    dragOver(e) {
        e.preventDefault();
        this.dragged.style.display = "none";
        if (e.target.className === 'placeholder') return;
        this.over = e.target;
        e.target.parentNode.insertBefore(placeholder, e.target);
    }

    render() {
        const { items1 } = this.state;
        var listItems1 = this.state.items1.map((item1, i1) => {
            return (
                <div
                    data-id={i1}
                    key={i1}
                    draggable='true'
                    onDragEnd={this.dragEnd.bind(this, item1)}
                    onDragStart={this.dragStart.bind(this)}>
                    <div draggable='true'>
                        <div name="name111" id="name1111">{item1.name1}</div>
                        <div name="id111" id="id1111">{item1.id1}</div>
                    </div>
                </div>
            )
        });
        const { items2 } = this.state;
        var listItems2 = this.state.items2.map((item2, i2) => {
            return (
                <div
                    data-id={i2}
                    key={i2}
                    draggable='true'
                    onDragEnd={this.dragEnd.bind(this, item2)}
                    onDragStart={this.dragStart.bind(this)}>
                    <div draggable='true'>
                        <div name="name222" id="name2222">{item2.name2}</div>
                        <div name="id222" id="id2222">{item2.id2}</div>
                    </div>
                </div>
            )
        });

        return (
            <div>
                <h1>Hello, this is dummy data for drag & drop.</h1>
                {/* <form> */}
                <div className='flex'>
                    <div className='flex-contained'>
                        <h3>This is the list of the yout Items1:</h3>
                        <div>
                            <div onDragOver={this.dragOver.bind(this)}>{listItems1}</div>
                        </div >
                        {/* <h3>Enter the Item1:</h3> */}
                        <div>
                            <div>
                                <label>Enter the Item1: </label>
                                <input
                                    type='text'
                                    name='name11'
                                    placeholder="Enter your item1"
                                    value={this.state.name11 || ''}
                                    onChange={this.addChange1}
                                />
                            </div>
                            <div>
                                <button onClick={this.onClickHandler1}>Add</button>
                            </div>
                        </div>
                    </div>
                    <div className="flex-contained">
                        <h3>This is the list of the yout Items2:</h3>
                        <div>
                            <div onDragOver={this.dragOver.bind(this)}>{listItems2}</div>
                        </div >
                        {/* <h3></h3> */}
                        <div>
                            <div>
                                <label>Enter the Item2: </label>
                                <input
                                    type='text'
                                    name='name22'
                                    placeholder="Enter your item2"
                                    value={this.state.name22 || ''}
                                    onChange={this.addChange2}
                                />
                            </div>
                            <div>
                                <button onClick={this.onClickHandler2}>Add</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default List2;