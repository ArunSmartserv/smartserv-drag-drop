import React from 'react';

class Test3 extends React.Component {

  constructor(props) {
    super(props);
    this.state = { lists: [] };
  }

  add() {
    var lists = [...this.state.lists];
    lists.push(this.newText.value);
    this.setState({ lists });
  }

  render() {
    return (
      <div className="list">
        <h1> creating list</h1>
        <input type="text" ref={(ip) => { this.newText = ip }} />
        <button onClick={this.add.bind(this)} className="btn1">Add
              </button>
        <ul>
          {this.state.lists.map(function (list) {
            return <li>{list}</li>
          })}

        </ul>
      </div>
    )
  }
};


export default Test3;
